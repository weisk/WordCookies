# Readme

## OBJECTIVE
Beat the game word-cookies

## TECHNOLOGIES

1. client
- react
- react-router
- redux + async actions
2. api
- express
- routes
- system.call to python script
3. python script: get all combinations from input string
4. webpack & webpack-dev-server
5. babel

## MODERN JS APIS
- async await
- import / export
- FS i/o (write, read)
- Util.Promisify callback functions
- Spread operator
- Half OOP: classes, static/class/instance properties & methods, inheritance, `this` binding
- HALF FP: Pure immutable state, Higher order functions, Thunks, Promises, Map/Reduce

## Docker compose

Define and run multi-container applications with Docker.

Usage:
  docker-compose [-f <arg>...] [options] [COMMAND] [ARGS...]
  docker-compose -h|--help

Options:
  -f, --file FILE             Specify an alternate Compose file (default: docker-compose.yml)


## Docker machine

$ docker-machine create --driver digitalocean --digitalocean-access-token=aa9399a2175a93b17b1c86c807e08d3fc4b79876545432a629602f61cf6ccd6b test-this

$ unset DOCKER_HOST DOCKER_MACHINE_NAME DOCKER_TLS_VERIFY DOCKER_CERT_PATH

## Server

valid token:
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiYUBhLmNvbSIsImlhdCI6MTUyMzQ3OTM4NiwiZXhwIjoxNTg1Njg3Mzg2fQ.sLVMfw1mztRePkqYAY4nCj0vxacWLAARbfZo63reirc

### generate sha256 hash
echo -r "b@b.com" | sha256sum

