import sys
from itertools import permutations

def perms(s):
    combos = []
    for i in range(3, len(s) + 1):
        combos.append([''.join(p) for p in permutations(s, i)])
    return combos

allcombos = perms(sys.argv[1])
print(allcombos);
