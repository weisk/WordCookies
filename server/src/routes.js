import chalk from 'chalk';
import express from 'express';
import body from 'body-parser';
import onHeaders from 'on-headers';
import prettyMs from 'pretty-ms';
import mongoose from 'mongoose';
import GoogleTranslate from 'google-translate-api';
import jwt from 'jsonwebtoken';
import expressListEndpoints from 'express-list-endpoints';

import _, {
  uniq
} from 'lodash';

import config from 'srv-config';

import {
  log,
  timeout,
  asyncWait,
  rightNow
} from 'srv-helpers';

import {
  unauthorized,
  badCombination,
  authenticate,
  isValidToken,
  signToken
} from './auth';

import words, {
  readBlacklist,
  writeBlacklist,
  dictionary
} from './wordcookies';

let db_conn;
mongoose.connect(process.env.MONGO, function(err, db) {
  if(err) { return console.dir(err); }
  db_conn = db;
  log(chalk.cyan(`[API] Mongo Connection OK`));
});

const routes = express.Router();
let blacklist = [];
readBlacklist()
.then(r => {
  blacklist = r;
  log('Blacklist ready, length: ', blacklist.length);
});

routes.use(body.json());

routes.use((req, res, next) => {
  let startAt = process.hrtime();
  onHeaders(res, () => {
    let diff = process.hrtime(startAt);
    let time = diff[0] * 1e3 + diff[1] * 1e-6;
    var ms = Math.round(time);
    log(`Took ${prettyMs(ms)}`);
  });

  next();
});

routes.use((req, res, next) => {
  log(`\n[${rightNow()}] ${req.method} Request to ${req.url}`);
  return next();
});

routes.get('/api/endpoints', (req, res, next) => {
  let endpoints = expressListEndpoints(routes);
  res.status(200).json(endpoints);
});

routes.get('/api/users', authenticate, (req, res, next) => {
  log('[middleware] users');
  db_conn.collection('users').find().toArray((err, users) => {
    res.status(200).json(users);
  });
});

routes.post('/api/login', async (req, res, next) => {
  let {
    user,
    pass
  } = req.body;
  log('[middleware] login');
  if (!user || !pass) {
    return badCombination(res);
  }

  db_conn
  .collection('users')
  .find({ email: { $eq: user } })
  .toArray()
  .then((users) => {
    return new Promise((resolve, reject) => {
      if (!users.length) {
        return reject();
      }

      let isValid = isValidToken(pass, users[0].password);
      if (!isValid) {
        return reject();
      }

      const data = { user: users[0].email };
      return resolve(data);
    });
  })
  .then((data) => {
    log(chalk.green(`[middleware] login SUCCESS for user ${data.user}`));
    return res.json({
      user: data.user,
      token: signToken(data)
    });
  })
  .catch((err) => {
    return badCombination(res);
  });
});

routes.get('/api/auth', authenticate, (req, res, next) => {
  log('[middleware] auth');
  return res.status(200).json({
    data: 'OK'
  });
});

routes.get('/api/combinations/:letters?/:length?/:startsWith?', authenticate, async (req, res, next) => {
// routes.get('/api/combinations/:letters?/:length?', async (req, res, next) => {
  log('[middleware] apiwords');
  if (!req.params || !req.params.letters) {
    return res.sendStatus(400);
  };

  let data = await words(req.params.letters, req.params.length);

  // reject blacklist
  // return res.json(_.chain(data)
  //     .reject((word) => _.includes(blacklist, word))
  //     .value());

  if (req.params.startsWith) {
    data = _.filter(data, word => _.startsWith(word, req.params.startsWith));
    return res.json(data)
  }

  return res.json(data);
});

routes.get('/api/blacklist', authenticate, async (req, res, next) => {
// routes.get('/api/blacklist', async (req, res, next) => {
  log('[middleware] blacklist get');
  res.json(blacklist);
});

routes.get('/api/dictionary', authenticate, async (req, res, next) => {
  log('[middleware] dictionary get');

  let data = dictionary();
  return res.json(data);
});


routes.post('/api/blacklist', authenticate, async (req, res, next) => {
// routes.post('/api/blacklist', async (req, res, next) => {
  let word = req.body.word;
  log('[middleware] blacklist post: ', word);
  if (!word) {
    return res.sendStatus(400);
  }

  blacklist.push(word);
  blacklist = uniq(blacklist);
  res.json(blacklist);
  await writeBlacklist(JSON.stringify(blacklist, null, 2));
});

routes.get('/api/gtranslate/:word', authenticate, async (req, res, next) => {
  let word = req.params.word;
  log('[middleware] gtranslate word: ', word);
  if (!word) {
    return res.sendStatus(400);
  }
  const translation = await GoogleTranslate(word, {to: 'es'});
  res.status(200).send(translation.text);
});

routes.use('/api/custom/:code', (req, res, next) => {
  log('[middleware] customcode');
  let code = req.params.code;
  res.status(code).end();
});

routes.use('/throw', (req, res, next) => {
  log('[middleware] throw');
  throw new Error('forced thrown error.');
});

routes.use('*', (req, res, next) => {
  log('[middleware] wildcard');
  return res.status(200).json({
    data: 'OK'
  });
});

export default routes;
