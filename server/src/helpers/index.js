import fs from 'fs';
import path from 'path';
import util from 'util';
import chalk from 'chalk';
import mkdirp from 'mkdirp';
import fetch from 'node-fetch';
import prettyMs from 'pretty-ms';
// import expressBunyan from 'express-bunyan-logger';

import config from '../config';

const read = util.promisify(fs.readFile);
const open = util.promisify(fs.open);

export function log(...rest) {
  if (process.env.DEBUG == 'true') {
    console.log(...rest);
  }
}

export function cors(req, res, next) {
  const origins = ['localhost'];
  const methods = ['GET', 'OPTIONS', 'POST', 'PUT', 'DELETE', 'PATCH'];
  const headers = ['Content-Type', 'Allow', 'Authorization', 'authorization'];
  //   const origins = config.cors.allowedOrigins;
  //   const methods = config.cors.allowedMethods;
  //   const headers = config.cors.allowedHeaders;

  // const origin = origins.indexOf(req.hostname);
  // const method = methods.indexOf(req.method);
  // if (origin < 0 || method < 0) {
  //   return res.sendStatus(401);
  // }

  // res.header("Access-Control-Allow-Origin", origins[origin]);
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", methods.join(','));
  res.header("Access-Control-Allow-Headers", headers.join(','));
  next();
}

export function timeout(s = 1) {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res();
    }, s * 1000);
  });
}

export function cls() {
  console.log('\x1Bc');
}

export function now() {
  const date = new Date();
  const day = date.getDay().toString().padStart(2, '0');
  const month = date.getMonth().toString().padStart(2, '0');
  const year = date.getFullYear().toString();
  const hours = date.getHours().toString().padStart(2, '0');
  const minutes = date.getMinutes().toString().padStart(2, '0');
  const seconds = date.getSeconds().toString().padStart(2, '0');
  return `${day}/${month}/${year} - ${hours}:${minutes}:${seconds}`;
}

export function rightNow() {
  const date = new Date();
  const hours = date.getHours().toString().padStart(2, '0');
  const minutes = date.getMinutes().toString().padStart(2, '0');
  const seconds = date.getSeconds().toString().padStart(2, '0');
  return `${hours}:${minutes}:${seconds}`;
}

export async function getJSON(path = '') {
  log(`Calling API ${path}`);
  const resp = await fetch(path);
  const json = await resp.json();
  return json;
}

export function asyncWait(ms = 1000) {
  return new Promise((res, rej) => setTimeout(res, ms));
}

export function getTimeDiff(hrtime) {
  let diff = hrtime[0] * 1e3 + hrtime[1] * 1e-6;
  let round = Math.round(diff);
  return prettyMs(round);
}

export function existsOrCreate(path) {
  read(path)
  .catch((err) => open(path, 'a'))
  .then(() => {
    console.log(chalk.cyan(`File ${path} created`));
  })
  .catch((err) => {
    console.log(chalk.red(`File ${path} couldnt be created`));
    console.error(err);
  });
}

export function logger() {
  const logsFolder = path.join(__dirname, '../../logs');
  const accessLog = path.join(logsFolder, 'access_log.log');
  const errorLog = path.join(logsFolder, 'error_log.log');
  const wsOptions = { flags: 'a', encoding: 'utf-8' };
  mkdirp.sync(logsFolder);

  return expressBunyan({
    streams: [{
      level: 'debug',
      stream: process.stdout
    }, {
      level: 'info',
      stream: fs.createWriteStream(accessLog, wsOptions)
    }, {
      level: 'error',
      stream: fs.createWriteStream(errorLog, wsOptions)
    }]
  })
};
