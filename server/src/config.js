export default {
  env: {
    log: false
  },
  port: process.env.API_PORT || 4000,
  mongoDb: process.env.MONGO
}
