import express from 'express';
import chalk from 'chalk';

import config from 'srv-config';
import {
  cors,
  log,
  timeout
} from 'srv-helpers';
import routes from './routes';

(async () => {
  log('starting app');
  const app = express();

  // app.use(helmet(config.helmet));
  // app.use(compression({threshold: 512}));
  // app.use(logger);
  app.use(cors);

  app.use(routes);

  app.listen(config.port, () => {
    console.log(chalk.cyan(`[API] OK, listening on port ${config.port}!`));
  });

  app.on('error', function onError(error) {
    if (error.syscall !== 'listen') {
      throw error;
    }

    const bind = typeof port === 'string' ?
      'Pipe ' + port :
      'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        console.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  });
})()
.catch((err) => {
  console.log('this is catched');
  console.error(err);
});
