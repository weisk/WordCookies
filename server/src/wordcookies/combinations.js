import fs from 'fs';
import path from 'path';
import {promisify} from 'util';

const readFile = promisify(fs.readFile);
const combsFile = './combs.json';


export default async function combinations(str = '') {
  // const arr = str.split('');
  // const combos = allcombinations(arr);

  // const results = [];
  // for (var combo of combos) {
  //   const comboStr = combo.join('');
  //   results.push(comboStr);
  // }
  const combsPath = path.join(__dirname, '..', combsFile);
  let contents = await readFile(combsPath, 'utf8');
  contents = contents.replace(/'/g, '"');

  return JSON.parse(contents);
}

