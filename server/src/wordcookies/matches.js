import fs from 'fs';
import path from 'path';
import {promisify} from 'util';
import _ from 'lodash';

const readFile = promisify(fs.readFile);
const dictFile = './english_dict-466k.txt';
// const dictFile = './english_dict-10k.txt';
const dictPath = path.join(__dirname, dictFile);

export default class Dict {
  constructor() {
    this.words = [];
    this.init();
  }

  init() {
    readFile(dictPath, 'utf8')
    .then((data) => data.split('\n'))
    .then((words) => words.map((w) => w.toLowerCase()))
    .then((words) => {
      this.words = words;
      console.log('Dictionary ready, length: ', words.length);
    })
  }

  getDict() {
    return this.words;
  }

  matches(arr) {
    return _.intersection(this.words, arr);
  }
}

