import _ from 'lodash';
import {promisify} from 'util';
import {exec} from 'child_process';
import {readFile, writeFile} from 'fs';
import path from 'path';

import combinations from './combinations';
import Dict from './matches';
import {
  log
} from 'srv-helpers';

const execute = promisify(exec);
const read = promisify(readFile);
const write = promisify(writeFile);

const blacklistPath = '../../data/blacklist.json';

const dict = new Dict();

function getpath(file) { return path.join(__dirname, file); }

function isVowel(s) { return (/^[aeiou]$/i).test(s); }

function isConsonant(s) { return (/^[^aeiou]$/i).test(s); }

async function generate(string) {
  return await execute(`python combs.py ${string} > src/combs.json`);
}

export async function readBlacklist() {
  try {
    let blacklist = await read(getpath(blacklistPath), 'utf8');
    return JSON.parse(blacklist);
  } catch (err) {
    return [];
  }
}

export async function writeBlacklist(data) {
  return await write(getpath(blacklistPath), data);
}

export default async function words(word, length, consonants = false) {
  const letters = word;
  const len = length || letters.length;

  await generate(letters);

  let combos = await combinations(letters);
  combos = _.flatten(combos);
  const matches = dict.matches(combos);

  let result;
  if (len) {
    result = _.chain(matches)
      .filter((e) => e.length == parseInt(len))
      .sortBy()
      .value();
  } else {
    result = matches;
  }

  return result;
}

export function dictionary() {
  const words = dict.getDict();
  return words;
}
