import fetch, {Headers} from 'node-fetch';

const baseUrl = 'https://api.datamuse.com/words?sp=';

const headers = new Headers({
  'app_id': 'e01fef4c',
  'app_key': '5827fd745b8c26e8b7c8ef1b325ce47a'
});

export default async function search(query) {
  const url = `${baseUrl}${query}`;

  const resp = await fetch(url);
  const json = await resp.json();

  return json;
}
