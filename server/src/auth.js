import chalk from 'chalk';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';

import {log} from 'srv-helpers';

const supersecret = 'wtf';

const getHexHash = (str = '') => {
  return crypto.createHash('sha256').update(str).digest('hex');
}

export function unauthorized(res) {
  log(chalk.red('[AUTH] unauthorized'));
  return res
    .header('WWW-Authenticate', 'Basic realm="User Area"')
    .status(403)
    .json({ success: false, message: 'Failed to authenticate'});;
}

export function badCombination(res) {
  log(chalk.red('[AUTH] bad combination'));
  return res.status(403).json({
    error: 'Username and password combination not found'
  });
}

export function authenticate(req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.headers['authorization'];
  log(chalk.cyan(`[AUTH] authenticate with token: ${token}`));

  if (!token) {
    return unauthorized(res)
  }

  jwt.verify(token, supersecret, function(err, decoded) {
    if (err) {
      return unauthorized(res);
    }
    return next();
  });
}

export function isValidToken(pass1, pass2) {
  const pass1_hash = getHexHash(pass1);
  const pass2_hash = pass2;
  return pass1_hash == pass2_hash;
}

export function signToken(str) {
  return jwt.sign(str, supersecret, {
    expiresIn: 60*60*24*7 // expires in 24 hours * 7 days
  });
}
