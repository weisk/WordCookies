const execute = require('child_process').exec;

// https://www.npmjs.com/package/depcheck

module.exports = (() => {
  const cmd = [
    "depcheck client/",
    "--specials=webpack,babel",
    "--ignore-dirs=build,docs,i18n,node_modules,public, server",
    "--ignores='",
      [
        'eslint*',
        'babel-*',
        'enzyme-*',
        '*-loader',
        'webpack*',
        'compression',
        'jest',
        'depcheck',
        'node-sass',
        'nodemon',
        'react-test-renderer',
        'redux-devtools-*',
      ].join(','),
    "'",
  ].join(" ");

  const child = execute(cmd);
  child.stdout.pipe(process.stdout);
})();
