import fs from 'fs';
import path from 'path';
import webpack from 'webpack';
import autoprefixer from 'autoprefixer';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import ExtractTextWebpackPlugin from 'extract-text-webpack-plugin'
import CompressionWebpackPlugin from 'compression-webpack-plugin';
import UglifyJsWebpackPlugin from 'uglifyjs-webpack-plugin';;
import WriteFileWebpackPlugin from 'write-file-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import _ from 'lodash';

import {envExpression} from 'utils/helpers';
import appConfig from './config.json';

const env = {
  DEBUG: process.env.DEBUG,
  API_URL: process.env.API_URL,
  APP_PORT: process.env.APP_PORT,
  APP_HOST: process.env.APP_HOST,
  APP_URL: process.env.APP_URL || 'http://localhost:3000',
  NODE_ENV: process.env.NODE_ENV
};

const envExp = envExpression(env);

console.log('webpack building');
console.log(env);

const extractStyles = new ExtractTextWebpackPlugin('[name].css', {
  allChunks: true,
  disable: !(process.env.NODE_ENV === 'production')
});
const extractHtml = new ExtractTextWebpackPlugin('[name].html', {
  allChunks: true
});

let config = {
  devtool: process.env.NODE_ENV === 'production' ? false : 'inline-source-map',
  devServer: {
    port: process.env.APP_PORT || 3000,
    host: process.env.APP_HOST || '0.0.0.0',
    disableHostCheck: true,
    historyApiFallback: {
      disableDotRule: true
    }
  },
  entry: {
    main: './src/index.jsx'
  },
  output: {
    filename: '[name].[hash:8].js',
    path: path.resolve(__dirname, 'build'),
    publicPath: '/',
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      loader: 'babel-loader',
      exclude: /node_modules/
    },{
      test: /\.(css|sass|scss)$/,
      use: [
        { loader: 'style-loader' },
        { loader: 'css-loader' },
        {
          loader: 'postcss-loader',
          options: {
            plugins: function() {
              autoprefixer({
                browsers: ['last 2 version', 'Explorer >= 10', 'Android >= 4']
              })
            }
          }
        },
        { loader: 'sass-loader' }
      ]
    },{
      test: /\.(jpe?g|png|gif|svg)$/i,
      loaders: [{
          loader: 'file-loader?context=public/images&name=images/[path][name].[ext]'
        },
        // {
        //   loader: 'image-webpack-loader',
        //   query: {
        //     mozjpeg: {
        //       progressive: true,
        //       quality: '65'
        //     },
        //     gifsicle: {
        //       interlaced: false,
        //     },
        //     optipng: {
        //       optimizationLevel: 4,
        //     },
        //     pngquant: {
        //       quality: '75-90',
        //       speed: 3,
        //     },
        //   },
        // }
      ],
      exclude: /node_modules/,
      include: __dirname
    }]
  },
  plugins: [
    new CleanWebpackPlugin('build'),
    new WriteFileWebpackPlugin(),
    new webpack.DefinePlugin(envExp),
    new HtmlWebpackPlugin({
      template: './public/index.html',
      inject: 'body',
      env: env,
      APP_URL: env.APP_URL,
      APP_TITLE: appConfig.appTitle
    }),
    new CopyWebpackPlugin([{
        from: 'public/favicon.ico'
      },
      {
        from: 'public/manifest.json'
      },
      {
        from: 'i18n/*.json'
      },
    ]),
    extractStyles,
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      'images': path.resolve(path.join(__dirname, 'public', 'images')),
      'actions': path.resolve(path.join(__dirname, 'src', 'actions')),
      'types': path.resolve(path.join(__dirname, 'src', 'types')),
      'components': path.resolve(path.join(__dirname, 'src', 'components')),
      'containers': path.resolve(path.join(__dirname, 'src', 'containers')),
    }
  }
};

if (process.env.NODE_ENV === 'production') {
  config.plugins = [
    ...config.plugins,
    new CompressionWebpackPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$|\.png$|\.jpe?g$|\.gif$/,
      threshold: 10240,
      minRatio: 0.8
    }),
    new UglifyJsWebpackPlugin({
      parallel: true
    })
  ];
} else {
  // config.module.rules.unshift({
  //   enforce: 'pre',
  //   test: /\.jsx?$/,
  //   exclude: /node_modules/,
  //   loader: 'eslint-loader',
  //   options: {
  //     emitError: true
  //   }
  // });
}

if (!process.env.VERBOSE) {
  const stats = {
    assets: true,
    assetsSort: 'size',
    chunks: false,
    children: false,
    modules: false,
    errors: true,
    errorDetails: true,
    excludeAssets: /\.(png|jpe?g|svg|gif|woff2?|ttf|eot)$/,
    warnings: true,
    hash: true,
    version: true,
    timings: true,
    reasons: true,
    source: true,
    publicPath: true
  };
  config.devServer.stats = {
    ...stats
  };
  config.stats = {
    ...stats
  };
};

export default config;
