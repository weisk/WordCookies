import React, { Component } from 'react';
import { Table } from 'antd';
import classNames from 'classnames';

import './List.scss';

export default class List extends Component {
  render() {
    const {
    columns,
      data,
  } = this.props;

    return (
      <Table
        className={classNames("list")}
        columns={columns}
        dataSource={data}
      />
    )
  }
};
