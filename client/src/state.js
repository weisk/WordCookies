export default {
  apiStats: {
    status: undefined,
    isFetching: true,
    lastUpdated: undefined,
  },
  blacklist: {
    items: [],
    isFetching: false,
    lastUpdated: undefined
  },
  general: {
   sidebarCollapsed: true,
  },
  user: {
    email: 'a@a.com',
    logged: false,
    // logged: true,
    // token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiYUBhLmNvbSIsImlhdCI6MTUyMTY2ODk5MCwiZXhwIjoyMzg1NjY4OTkwfQ.BQ8SW3WThu3aMR-UcKgS677JhVOi2hIkRyOYQZbPDcA'
  },
  wordsForm: {
    letters: '',
    length: 0
  }
};
