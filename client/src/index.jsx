import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import { addLocaleData, IntlProvider } from 'react-intl';
import Cookie from 'js-cookie';
import { Switch, Route, Redirect } from 'react-router-dom';
import { compose, createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import persistState from 'redux-localstorage';

import registerServiceWorker from './registerServiceWorker';
import rootReducer from './reducers'
import initialState from './state';

import Dashboard from 'containers/Dashboard';
import Login from 'containers/Login';
import Logout from 'containers/Logout';
import DummyPage from 'containers/DummyPage';

import {
  Loader,
  NoMatch,
} from 'components';

import {
  PropsRoute,
  PrivateRoute
} from './utils/routes';

import reactIntlLocaleEn from 'react-intl/locale-data/en';
import reactIntlLocaleEs from 'react-intl/locale-data/es';
const locales = {
  en: require('i18n/en.json'),
  es: require('i18n/es.json')
};

// bypass css modules by manually specifying loaders
// import '!style-loader!css-loader!sass-loader!./index.scss';
import './index.scss';

let devTools;
if (window && typeof window.__REDUX_DEVTOOLS_EXTENSION__ == 'function') {
  devTools = window.__REDUX_DEVTOOLS_EXTENSION__();
} else {
  devTools = function(){};
}

const store = createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(
      thunkMiddleware, // lets us dispatch() functions
      createLogger(), // neat middleware that logs actions
    ),
    persistState(['general', 'wordsForm', 'user'], {}),
    // MOBILE bug: https://github.com/elgerlambert/redux-localstorage/issues/73
    // devTools,
  )
);

const locale = Cookie.get('locale') || 'en';

function renderRoot(localeData) {
  ReactDOM.render(
    <Provider store={store}>
      <IntlProvider locale={locale} messages={localeData}>
        <LocaleProvider locale={enUS}>
          <BrowserRouter>
            <Switch>
              <Route exact path="/" render={() => (<Redirect to="/dashboard/words" />)} />
              <PrivateRoute path="/dashboard" component={Dashboard} locale={locale} redirectTo="/login"/>
              <PropsRoute path="/login" component={Login} />
              <PropsRoute path="/logout" component={Logout} />
              <PropsRoute path="/loader" component={Loader} spinning={true} fullScreen={true} />
              <Route path="/public" component={DummyPage} />
              <Route component={NoMatch} />
            </Switch>
          </BrowserRouter>
        </LocaleProvider>
      </IntlProvider>
    </Provider>
    , document.getElementById('root'));
}

let localeData = locales[locale] || locales['en'];

addLocaleData([
  ...reactIntlLocaleEn,
  ...reactIntlLocaleEs
]);
renderRoot(localeData);
registerServiceWorker();

