export default function testEnhancer() {
  return function (next) {
    return function (reducer, initialState, enhancer) {
      let store = next(reducer, initialState, enhancer);
      store.subscribe(function() {
        let state = store.getState();
        debugger;
      });
      return store;
    }
  }
}
