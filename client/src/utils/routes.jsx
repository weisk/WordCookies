import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

// based on https://github.com/ReactTraining/react-router/issues/4105#issuecomment-289195202

const renderMergedProps = (component, ...rest) => {
  const finalProps = Object.assign({}, ...rest);
  return (
    React.createElement(component, finalProps)
  );
};

const PropsRoute = ({ component, ...rest }) => {
  return (
    <Route {...rest} render={routeProps => {
      return renderMergedProps(component, routeProps, rest);
    }} />
  );
};

const DumbPrivateRoute = ({ user, component, redirectTo, ...rest }) => {
  return (
    <Route {...rest} render={routeProps => {
      return user.logged ? (
        renderMergedProps(component, routeProps, rest)
      ) : (
          <Redirect to={{
            pathname: redirectTo,
            state: { from: routeProps.location }
          }} />
        );
    }} />
  );
};

const mapStateToProps = state => ({
  user: state.user
});

const PrivateRoute = connect(mapStateToProps)(DumbPrivateRoute);

export {
  PropsRoute,
  PrivateRoute
};
