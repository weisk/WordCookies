import 'isomorphic-fetch';

export default class DoFetch() {
  constructor(config = {}) {
    this.config = config;
  }

  setConfig(config) {
    this.config = {
      ...this.config,
      config;
    }
  }

  doFetch(url, options) {
    return fetch(url, {
      ...this.config,
      options;
    });
  }
}
