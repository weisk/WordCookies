import _ from 'lodash';

const isUppercase = (char) => /[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/.test(char);

export function pickOnlyUppercase(obj={}) {
  return _.pickBy(obj, (v,k) => _.every(k, isUppercase))
}

export function envExpression(obj={}) {
  return _.chain(obj)
    .map((v, k) => {
      return {
        [`process.env.${k}`]: JSON.stringify(v)
      };
    })
    .reduce((acc, next) => {
      return Object.assign(acc, next)
    }, {})
    .value();
}
