import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Cookie from 'js-cookie';
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Button,
  Row,
  Form,
  Input,
  message,
} from 'antd';

import { userActions } from 'actions';
import './Login.scss';

const FormItem = Form.Item;

class Login extends Component {
  static propTypes = {
    loading: PropTypes.object,
    userActions: PropTypes.object,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  }

  onSubmit(event) {
    event.preventDefault();
    const user = this.refs.user.value;
    const pass = this.refs.pass.value;

    this.props.userActions.login({ user, pass })
    .then((r) => {
      if (r && r.logged == true) {
        Cookie.set('Authorization', r.token);
        return this.props.history.push('/dashboard');
      }
      throw new Error();
    })
    .catch((err) => {
      message.error('Invalid credentials');
    });
  }

  setUsername(evt) {
    // this.setState({ letters: evt.target.value });
    this.props.userActions.set({
      email: evt.target.value
    });
  }

  // setPassword(evt) {
  //   // this.setState({ letters: evt.target.value });
  //   this.props.userActions.set({
  //     pass: evt.target.value
  //   });
  // }

  render() {
    const {
      loading,
      user,
      history
    } = this.props;

    return (
      <div>
        <div style={{'margin': '30px', 'backgroundColor': 'black', 'padding': '15px'}}>
          <Link to={'/public'}>
            <h3 style={{ 'color': 'pink'}}>This link doesn't require auth</h3>
          </Link>
        </div>
        <br/>
        <br/>

        <div className="form">
          <div className="logo">
            <img src={require('images/step-logo-white.svg')} alt="Logo" />
            <span>Conference CMS</span>
          </div>
          <Form onSubmit={(event) => this.onSubmit(event)}>
            <FormItem hasFeedback>
              <input
                ref="user"
                type="text"
                placeholder="Username"
                value={user.email}
                onChange={(evt) => this.setUsername(evt)}
                required/>
            </FormItem>
            <FormItem hasFeedback>
              <input
                ref="pass"
                type="password"
                placeholder="Password"
                required
                onChange={(evt) => {}}/>
            </FormItem>
            <Row>
              <Button type="primary" htmlType="submit" size="large" loading={loading}>
                Sign in
              </Button>
            </Row>
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  userActions: bindActionCreators(userActions, dispatch)
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Login));
