import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom';
import { Button, Row, Form, Input } from 'antd';

import { userActions } from 'actions';
import './Logout.scss';

const FormItem = Form.Item;

class Logout extends Component {
  static propTypes = {
    loading: PropTypes.object,
    userActions: PropTypes.object,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  }

  handleOk() {
    this.props.history.push('/login');
  }

  render() {
    const {
      loading,
    } = this.props;

    return (
      <div>
        <h3>You have successfully logged out.</h3>
        <form>
          <Row>
            <Button type="primary" size="large" onClick={() => this.handleOk()} loading={loading}>
              Login
            </Button>
          </Row>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  userActions: bindActionCreators(userActions, dispatch)
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Logout));
