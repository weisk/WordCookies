import 'isomorphic-fetch';
import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import NumericInput from 'react-numeric-input';
import {
  Form,
  Input,
  Icon,
  Button,
  Checkbox,
  Popconfirm,
  message
} from 'antd';

import {
  userActions,
  blacklistActions,
  wordsFormActions
} from 'actions';

import './Words.scss';

const FormItem = Form.Item;
const initialState = {
  formStatus: 'All ok!',
  consonants: true,
  loading: false,
  words: [],
  hidden: [],
  applyFilters: true
};

function isConsonant(s) { return (/^[^aeiouy]$/i).test(s); }

class Words extends Component {
  constructor() {
    super();
    this.state = initialState;
  }

  getWords(event) {
    event.preventDefault();

    const {
      wordsForm: {
        letters,
        length
      },
      user: {
        token
      }
     } = this.props;

    const {
      consonants
    } = this.state;

    if (!letters) {
      return this.setState({ formStatus: 'Please input some letters'});
    } else if (letters.length < 4) {
      return this.setState({ formStatus: 'Please minimum 4 letters'});
    } else if (letters.length > 8) {
      return this.setState({ formStatus: 'Please maximum 8 letters'});
    }

    const word = letters;
    let n = parseInt(length);
    if (!n) {
      n = word.length;
      this.props.wordsFormActions.set({ length: n });
    } else if (n > word.length) {
      n = word.length
    }

    this.setState({ loading: true });

    let partialUrl = `${process.env.API_URL}/api/combinations/${word}/${n}`;
    fetch(partialUrl, {
      headers: {
        Authorization: token
      }
    })
    .then(res => res.json())
    .then(data => {
      let newData = data;
      let hidden = [];
      if(consonants) {
        newData = _.filter(newData, (e) => {
          return !_.every(e, isConsonant);
        });
        hidden = _.difference(data, newData);
      }

      this.setState({
        words: newData,
        hidden,
        loading: false
      })
    })
    .catch((err) => {
      this.setState({ loading: false });
    });
  }

  addToBlacklist(event, word) {
    const { blacklist } = this.props;
    event.preventDefault();
    event.stopPropagation();
    if (!word) {
      return;
    }
    this.props.blacklistActions.blacklistSet(word);
  }

  openGoogle(event, word) {
    const { blacklist } = this.props;
    event.preventDefault();
    event.stopPropagation();
    if (!word) {
      return;
    }
    window.open(`https://translate.google.com/#en/es/${word}`, '_blank');
  }

  setLetters(evt) {
    // this.setState({ letters: evt.target.value });
    this.props.wordsFormActions.set({
      letters: evt.target.value
    });
  }

  setLength(val) {
    this.props.wordsFormActions.set({
      length: val
    });
  }

  setConsonants(evt) { this.setState({ consonants: evt.target.checked }); }

  setApplyFilters(evt) { this.setState({ applyFilters: evt.target.checked }); }

  removeWord(evt, word) {
    event.preventDefault();
    const { words, hidden } = this.state;
    if (!word || _.isEmpty(word)) {
      return
    }

    console.log(`Adding ${word} to hidden`);
    this.setState({
      words: _.without(words, word),
      hidden: [
        ...hidden,
        word
      ]
    });
  }

  reset(event) {
    event.preventDefault();
    this.setState({
      ...initialState,
    });
    this.props.wordsFormActions.reset();
  }

  filterWords(words) {
    const { hidden, applyFilters } = this.state;
    const { blacklist } = this.props;
    return _.chain(words)
      .reject((word) => applyFilters && _.includes(blacklist.items, word))
      .reject((word) => applyFilters && _.includes(hidden, word))
      .value();
  }

  blacklistAllWords() {
    const { words } = this.state;
    let filteredWords = this.filterWords(words);
    Promise.all(_.map(filteredWords, (word) => {
      return this.props.blacklistActions.blacklistSet(word);
    }))
    .then(() => {
      console.log('Done!');
      message.success('Done');
      // this.props.blacklistActions.blacklistFetch();
    });
  }

  cancel(e) {
    console.log(e);
    message.error('Click on No');
  }

  render() {
    const {
      user,
      blacklist,
      wordsForm: {
        letters,
        length
      }
     } = this.props;

    const { formStatus, words, consonants, loading, hidden, applyFilters} = this.state;

    let filteredWords = this.filterWords(words);
    let rejectedWords = _.filter(words, (word) => _.includes(blacklist.items, word));

    return (
      <div className='page-words bold'>
        <h2>Words</h2>
        <hr/>
        { formStatus &&
          <pre>{ formStatus }</pre>
        }
        <br/>
        <Form
          layout="inline"
          onSubmit={(event) => this.getWords(event)}
          ref={(input) => this.wordForm = input} >
          <FormItem>
            <input
              type="text"
              placeholder="Letters"
              value={letters}
              onChange={(evt) => this.setLetters(evt)}
              autoFocus/>
          </FormItem>
          <FormItem>
            <NumericInput
              placeholder="Length"
              value={length}
              onChange={(val) => this.setLength(val)}
              style={{
                input: {
                  height: "45px"
                }
              }}/>
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit">
              Get
            </Button>
          </FormItem>
          <FormItem>
            <Button onClick={(evt) => {this.reset(evt)}}>
              Reset
            </Button>
          </FormItem>
          <br/>
          <FormItem>
            <Checkbox
              placeholder="Consonants"
              checked={consonants}
              onChange={(evt) => this.setConsonants(evt)}>
              Reject Consonant-Only
            </Checkbox>
          </FormItem>
          <FormItem>
            <Checkbox
              placeholder="Filters"
              checked={applyFilters}
              onChange={(evt) => this.setApplyFilters(evt)}>
              Apply filters
            </Checkbox>
          </FormItem>
          {
            loading && <Icon type="loading" />
          }
        </Form>
        <br/>
        {/* 9PX PER LLETRA */ }
        <p>Words: {filteredWords.length}</p>
        <div className="combinations">
          {filteredWords.map((el, i) =>
            <Button
              className="comb"
              key={i}
              onClick={(evt) => this.removeWord(evt, el)}
              style={{ display: 'flex' }}>
                <span>{el}</span>
                <div className="spacer-horizontal-m"></div>
                <div onClick={(evt) => this.addToBlacklist(evt, el)} title="Add to blacklist">
                  <Icon type="delete" className="red"></Icon>
                </div>
                <div onClick={(evt) => this.openGoogle(evt, el)} title="Google Translate">
                  <Icon type="rollback" className="green" style={{transform: "rotateZ(90deg)"}}/>
                </div>
            </Button>
          )}
        </div>
        <br/>
        <br/>

        <Popconfirm
          title="Are you sure?"
          onConfirm={(e) => this.blacklistAllWords(e)}
          onCancel={(e) => this.cancel(e)}
          okText="Yes"
          cancelText="No">
          {/*<span className="comb all" onClick={() => this.blacklistAllWords()}>Blacklist All</span>*/}
          <span className="comb all">Blacklist All</span>
        </Popconfirm>
        <hr/>
        <div>
          <span><b># Words:</b> { filteredWords.length}</span>
          <br/>
          <span><b># Blacklisted:</b> { rejectedWords.length}</span>
          <br/>
          <span><b># Hidden:</b> { hidden.length}</span>
        </div>
        <hr/>
        <div className="combinations">
          {rejectedWords.map((el, i) =>
            <span className="comb" key={i}>{el}</span>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { user, blacklist, wordsForm } = state;
  return {
    user,
    blacklist,
    wordsForm
  }
};

const mapDispatchToProps = dispatch => ({
  userActions: bindActionCreators(userActions, dispatch),
  blacklistActions: bindActionCreators(blacklistActions, dispatch),
  wordsFormActions: bindActionCreators(wordsFormActions, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Words);
