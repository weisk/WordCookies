import React from 'react';
import { Link } from 'react-router-dom';

// dummy public route
export default function DummyPage() {
  return (
    <div>
      <h2>You can be here,</h2>
      <h3>wether you are logged in or not.</h3>
      <Link to="/dashboard">Try to go to the dashboard without logging?</Link>
    </div>
  );
}
