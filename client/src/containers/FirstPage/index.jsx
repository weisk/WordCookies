import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import {
  DatePicker,
  message,
} from 'antd';

import { userActions } from 'actions';

import './FirstPage.scss';

class FirstPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: '',
    };
  }

  handleChange(date) {
    message.info('Selected Date: ' + date.toString());
    this.setState({ date });
  }

  render() {
    const {
      user,
      apiStats
    } = this.props;
    return (
      <div className='bold'>
        <h2>
          <FormattedMessage id="app.hello" />
        </h2>
        <DatePicker onChange={value => this.handleChange(value)} />
        <div style={{ marginTop: 20 }}>Date: {this.state.date.toString()}</div>
        <h2>First Page</h2>
        <p>{`Email: ${user.email}`}</p>
        <br/>
        { apiStats.isFetching ?
          <h2>Loading...</h2> :
          <div>
            <h2>Api Stats:</h2>
            <pre>{JSON.stringify(apiStats, null, 2)}</pre>
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  apiStats: state.apiStats
});

const mapDispatchToProps = dispatch => ({
  userActions: bindActionCreators(userActions, dispatch)
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(FirstPage));
