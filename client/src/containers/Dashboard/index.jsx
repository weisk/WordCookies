import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Cookie from 'js-cookie';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import { Switch, Route, Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import {
  Layout,
  Menu,
  Icon,
  Breadcrumb,
  Button,
  Dropdown,
} from 'antd';
const { Header, Content, Footer, Sider } = Layout;

import FirstPage from 'containers/FirstPage';
import SecondPage from 'containers/SecondPage';
import Words from 'containers/Words';
import BlackList from 'containers/BlackList';
import Define from 'containers/Define';
import Dictionary from 'containers/Dictionary';
import {
  generalActions,
  apiStatsActions,
  blacklistActions,
  userActions
} from 'actions';

import './App.scss';

class Dashboard extends Component {
  static propTypes = {
    locale: PropTypes.string,
    generalActions: PropTypes.object,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    dispatch: PropTypes.func.isRequired
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(this.props.apiStatsActions.apiStatsFetch);
    dispatch(this.props.blacklistActions.blacklistFetch);
  }

  toggle() {
    this.props.generalActions.set({
      sidebarCollapsed: !this.props.general.sidebarCollapsed
    });
  }

  changeLocale(locale = 'en') {
    Cookie.set('locale', locale.key);
    window.location.reload();
  }

  logout() {
    this.props.userActions.reset();
    this.props.history.push('/logout');
  }

  render() {
    const {
      locale,
      general,
      blacklist,
    } = this.props;

    const languagesMenu = (
      <Menu onClick={(locale) => this.changeLocale(locale)}>
        <Menu.Item key="en">
          <FormattedMessage id="app.english" />
        </Menu.Item>
        <Menu.Item key="es">
          <FormattedMessage id="app.spanish" />
        </Menu.Item>
      </Menu>
    );

    const userMenu = (
      <Menu>
        <Menu.Item key="0">
          <a href="http://www.alipay.com/">My Profile</a>
        </Menu.Item>
        <Menu.Item key="1">
          <a href="http://www.taobao.com/">Notifications</a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3">
          <span onClick={() => this.logout()}>Logout</span>
        </Menu.Item>
      </Menu>
    );

    // let defaultSelectedKeys = ['a1'];
    let defaultSelectedKeys = [];
    // let defaultOpenKeys = ['a', 'b'];
    let defaultOpenKeys = [];

    return (
      <Layout className="layout">
        <Sider
          className="sider"
          trigger={null}
          collapsible
          collapsed={general.sidebarCollapsed}>

          <div className="logo">
            <Link to={'/'}>
              <img src={require('images/step-logo-white.svg')} alt="Logo" />
            </Link>
          </div>

          <Menu
            theme="dark"
            mode="inline"
            defaultSelectedKeys={defaultSelectedKeys}
            defaultOpenKeys={defaultOpenKeys}>
            <Menu.SubMenu key="a" title={<span><Icon type="home" /><span>Event Menu</span></span>}>
              <Menu.Item key="a1">
                <Link to={'/dashboard'}>
                  <Icon type="user" />
                  <span>First</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="a2">
                <Link to={'/dashboard/second'}>
                  <Icon type="video-camera" />
                  <span>Second</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="a3">
                <Link to={'/dashboard/words'}>
                  <Icon type="video-camera" />
                  <span>Words</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="a4">
                <Link to={'/dashboard/blacklist'}>
                  <Icon type="video-camera" />
                  <span>Blacklist</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="a5">
                <Link to={'/dashboard/dictionary'}>
                  <Icon type="video-camera" />
                  <span>Dictionary</span>
                </Link>
              </Menu.Item>
            </Menu.SubMenu>

            <Menu.SubMenu key="b" title={<span><Icon type="tool" /><span>Tools</span></span>}>
              <Menu.Item key="b1">
                <Link to={'/dashboard/define'}>
                  <Icon type="tags" />
                  <span>Define</span>
                </Link>
              </Menu.Item>
            </Menu.SubMenu>
          </Menu>
        </Sider>

        <Layout>
          <Header className="header">
            <Icon
              className="trigger"
              type={general.sidebarCollapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={() => this.toggle()} />
            <div className="flex" />

            <Dropdown overlay={languagesMenu} placement="bottomRight" trigger={['click']}>
              <Button style={{ marginLeft: 8 }}>
                {locale} <Icon type="down" />
              </Button>
            </Dropdown>

            <div className="spacer-horizontal-m" />

            <Dropdown overlay={userMenu} placement="bottomRight" trigger={['click']}>
              <a className="ant-dropdown-link" href="something">
                John Doe <Icon type="down" />
              </a>
            </Dropdown>

          </Header>

          <Content style={{ padding: '0 25px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>List</Breadcrumb.Item>
              <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb>

            <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>
              <Route exact path="/dashboard" component={FirstPage} />
              <Route exact path="/dashboard/second" component={SecondPage} />
              <Route exact path="/dashboard/words" component={Words} />
              <Route exact path="/dashboard/blacklist" component={BlackList} />
              <Route exact path="/dashboard/dictionary" component={Dictionary} />
              <Route exact path="/dashboard/define" component={Define} />
            </div>

            <div style={{ margin: '16px 0' }} />
          </Content>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  general: state.general,
  apiStats: state.apiStats,
  blacklist: state.blacklist
});

const mapDispatchToProps = dispatch => ({
  generalActions: bindActionCreators(generalActions, dispatch),
  apiStatsActions: bindActionCreators(apiStatsActions, dispatch),
  blacklistActions: bindActionCreators(blacklistActions, dispatch),
  userActions: bindActionCreators(userActions, dispatch)
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard));
