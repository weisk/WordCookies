import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Cookie from 'js-cookie';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import { Switch, Route, Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import {
  Layout,
  Menu,
  Icon,
  Breadcrumb,
  Button,
  Dropdown
} from 'antd';
const { Header, Content, Footer } = Layout;

import FirstPage from 'containers/FirstPage';
import SecondPage from 'containers/SecondPage';
import Words from 'containers/Words';
import BlackList from 'containers/BlackList';
import Define from 'containers/Define';

import {
  generalActions,
  apiStatsActions,
  blacklistActions
} from 'actions';
import './App.scss';


class App extends Component {
  static propTypes = {
    locale: PropTypes.string,
    generalActions: PropTypes.object,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    dispatch: PropTypes.func.isRequired
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(this.props.apiStatsActions.apiStatsFetch);
    dispatch(this.props.blacklistActions.blacklistFetch);
  }

  toggle() {
    this.props.generalActions.set({
      sidebarCollapsed: !this.props.general.sidebarCollapsed
    });
  }

  changeLocale(locale = 'en') {
    Cookie.set('locale', locale.key);
    window.location.reload();
  }

  logout() {
    this.props.userActions.reset();
    this.props.history.push('/logout');
  }

  render() {
    const {
      locale,
      general,
      blacklist,
    } = this.props;

    const languagesMenu = (
      <Menu onClick={(locale) => this.changeLocale(locale)}>
        <Menu.Item key="en">
          <FormattedMessage id="app.english" />
        </Menu.Item>
        <Menu.Item key="es">
          <FormattedMessage id="app.spanish" />
        </Menu.Item>
      </Menu>
    );

    const userMenu = (
      <Menu>
        <Menu.Item key="0">
          <a href="http://www.alipay.com/">My Profile</a>
        </Menu.Item>
        <Menu.Item key="1">
          <a href="http://www.taobao.com/">Notifications</a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3">
          <span onClick={() => this.logout()}>Logout</span>
        </Menu.Item>
      </Menu>
    );

    // let defaultSelectedKeys = ['a1'];
    let defaultSelectedKeys = ['2'];
    // let defaultOpenKeys = ['a', 'b'];
    let defaultOpenKeys = [];

    return (
      <Layout className="page-layout">
      <Header>
        <div className="logo">
          <img src={require('images/step-logo-white.svg')} alt="Logo" />
        </div>
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={defaultSelectedKeys}
          style={{ lineHeight: '64px' }}
        >
          <Menu.Item key="1">
            <Link to={'/dashboard'}>
              <Icon type="home" />
              <span>Home</span>
            </Link></Menu.Item>
          <Menu.Item key="2">
            <Link to={'/dashboard/words'}>
              <Icon type="form" />
              <span>Words</span>
            </Link>
          </Menu.Item>
          <Menu.Item key="3">
            <Link to={'/dashboard/define'}>
              <Icon type="form" />
              <span>Define</span>
            </Link>
          </Menu.Item>
          <Menu.Item key="4">
            <Link to={'/dashboard/blacklist'}>
              <Icon type="bars" />
              <span>Blacklist</span>
            </Link>
          </Menu.Item>
        </Menu>
      </Header>
      <Content style={{ padding: '0 50px' }}>
        <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
          <Switch>
            <Route exact path="/dashboard" component={FirstPage} />
            <Route exact path="/dashboard/second" component={SecondPage} />
            <Route exact path="/dashboard/words" component={Words} />
            <Route exact path="/dashboard/blacklist" component={BlackList} />
            <Route exact path="/dashboard/define" component={Define} />
          </Switch>
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>
        Ant Design ©2016 Created by Ant UED
      </Footer>
    </Layout>
    );
  }
}

const mapStateToProps = state => ({
  general: state.general,
  apiStats: state.apiStats,
  blacklist: state.blacklist
});

const mapDispatchToProps = dispatch => ({
  generalActions: bindActionCreators(generalActions, dispatch),
  apiStatsActions: bindActionCreators(apiStatsActions, dispatch),
  blacklistActions: bindActionCreators(blacklistActions, dispatch)
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(App));
