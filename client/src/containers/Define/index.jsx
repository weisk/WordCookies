import 'isomorphic-fetch';
import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
// import GoogleTranslate from 'google-translate-api';
// import cheerio from 'cheerio';

import {
  Button,
  Spin,
  Radio,
  Icon
} from 'antd';

import './Define.scss';

const RadioGroup = Radio.Group;
const MIN_LEN = 3;
const MAX_LEN = 10;

class Define extends Component {
  constructor(){
    super();
    this.state = {
      status: 1,
      query: 'ail',
      results: undefined,
      radio: 2
    }
  }

  onChangeHandler(e){
    this.setState({ query: e.target.value});
  }

  onChangeRadio(e) {
    this.setState({ radio: e.target.value, results: undefined});
  }

  isValid(query) {
    return typeof query == 'string' &&
      !_.isEmpty(query) &&
      query.length >= MIN_LEN &&
      query.length <= MAX_LEN;
  }

  search(e){
    const { radio } = this.state;
    const { input } = this.refs;
    const query = input.value;
    const isValid = this.isValid(query);

    if (!isValid) {
      return;
    }

    switch(radio) {
      case 1:
        this.getFromUD(query);
        break;
      case 2:
        this.getFromGoogle(query);
        break;
    }
  }

  google(e){
    const { input } = this.refs;
    const query = input.value;
    if (query) {
      window.open(`https://translate.google.com/#en/es/${query}`, '_blank');
    }
  }

  getFromUD(query) {
    this.setState({ status: 2});
    fetch(`http://api.urbandictionary.com/v0/define?term=${query}`)
    .catch((err) => this.setState({ loading: false }))
    .then(res => res.json())
    .then(data => {
      this.setState({
        status: 3,
        results: data
      });
    });
  }

  getFromGoogle(query) {
    const { token } = this.props.user;;
    const cfg = {
      headers: {
        Authorization: token
      }
    };
    this.setState({ status: 2});
    fetch(`${process.env.API_URL}/api/gtranslate/${query}`, cfg)
    .then(res => res.text())
    .then((data) => {
      this.setState({
        status: 3,
        results: data
      });
    });

  }

  renderResults(status, results) {
    const { radio } = this.state;
    if (status !== 3 || !results) {
      return;
    }

    switch(radio) {
      case 1:
        if (!results || !results.list || !results.list.length) {
          return;
        }
        return (
          <ul>
            {results.list.map((item, i) => {
              return (
                <li key={i}>{item.definition}</li>
              );
            })}
          </ul>
        );
        break;
      case 2:
        return (
          <p>{ results }</p>
        );
    }

  }

  reset() {
    this.setState({
      status: 1,
      query: '',
      results: []
    });
  }

  render() {
    const { query, status, results, radio} = this.state;
    const { blacklist } = this.props;

    return (
      <div className="page-define">
        <h2>Define word</h2>
        <div className="spacer-vertical-m"></div>
        <div className="row">
          <label>Word:</label>
          <div className="spacer-horizontal-s"></div>
          <input ref="input" value={this.state.query}  onChange={(event) => this.onChangeHandler(event)}/>
          <div className="spacer-horizontal-m"></div>
          <Button onClick={(evt) => {this.search(evt)}}>Search</Button>
          <div className="spacer-horizontal-m"></div>
          <Button onClick={(evt) => {this.reset(evt)}}>Reset</Button>
          <div className="spacer-horizontal-m"></div>
          <Button onClick={(evt) => {this.google(evt)}}>
            Google
            <Icon type="rollback" style={{transform: "rotateZ(90deg)"}}/>
          </Button>
          <div className="spacer-horizontal-m"></div>
          { status == 2 && <Spin/> }
        </div>
        <div className="row">
          <RadioGroup onChange={(event) => this.onChangeRadio(event)} value={radio}>
            <Radio value={1}>UrbanDictionary</Radio>
            <Radio value={2}>GoogleTranslate</Radio>
          </RadioGroup>
        </div>
        <div className="spacer-vertical-l"></div>
        <div className="results">{
          this.renderResults(status, results)
        }</div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    blacklist: state.blacklist,
    user: state.user
  }
};

export default connect(mapStateToProps)(Define);
