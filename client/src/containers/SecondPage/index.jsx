import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { Button } from 'antd';

import { userActions, apiStatsActions } from 'actions';
import './SecondPage.scss';

class SecondPage extends Component {
  setUser() {
    const user = this.newUser.value;
    if (!user || user === "") {
      return;
    }

    this.props.userActions.set({ email: user });
  }

  render() {
    const { user, apiStats } = this.props;

    return (
      <div className='bold'>
        <h2>Second Page</h2>
        <p>{ user.email }</p>
        <input ref={(input) => this.newUser = input}/>
        <Button onClick={() => this.setUser()}>Change user</Button>
        <br/>
        <br/>
        <h2>Api stats:</h2>
        <table>
          <thead>
            <tr>
              <th>type</th>
              <th>started</th>
              <th>uptime</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{ apiStats.type }</td>
              <td>{ apiStats.started }</td>
              <td>{ apiStats.uptime }</td>
            </tr>
          </tbody>
        </table>
        <br/>
        <br/>
        <Link to={'/dashboard'}>First</Link>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { user, apiStats } = state;
  const {
    isFetching,
  } = apiStats || {
    isFetching: true
  }

  return {
    user,
    isFetching,
    apiStats,
  }
};

const mapDispatchToProps = dispatch => ({
  userActions: bindActionCreators(userActions, dispatch),
  apiStatsActions: bindActionCreators(apiStatsActions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SecondPage);
