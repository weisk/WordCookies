import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination } from 'antd';

import './BlackList.scss';

class BlackList extends Component {
  constructor(){
    super();
    this.state = {
      input: '',
      currentPage: 0,
      pageSize: 50
    }
  }

  onChangeHandler(e){
    this.setState({
      input: e.target.value,
    });
  }

  onChangePage(page = 0) {
    this.setState({
      currentPage: page - 1
    });
  }

  render() {
    const { currentPage, pageSize } = this.state;

    const start = currentPage * pageSize;

    const filtered = this.props.blacklist.items
      .filter((word) => {
        return this.state.input === '' || word.includes(this.state.input)
      });

    const sliced = filtered.slice(start, start + pageSize);

    return (
      <div className="page-blacklist">
        <h2>Current black list</h2>
        <div className="spacer-vertical-m"></div>
        <div className="row">
          <label>Filter:</label>
          <div className="spacer-horizontal-s"></div>
          <input value={this.state.input} type="text" onChange={(event) => this.onChangeHandler(event)}/>
          <div className="spacer-horizontal-m"></div>
          <div className="col">
            <p>Total: {this.props.blacklist.items.length}</p>
            <p>Filtered: {filtered.length}</p>
          </div>
        </div>
        <hr/>
        <div style={{display: 'flex', justifyContent: 'flex-end'}}>
          <Pagination
            pageSize={pageSize}
            total={filtered.length}
            currentPage={currentPage}
            onChange={(e) => this.onChangePage(e)}/>
        </div>
        <br/>
        <div className="combinations">
          {sliced.map((word, i) =>
            <span className="comb" key={i}>{word}</span>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    blacklist: state.blacklist
  }
};

export default connect(mapStateToProps)(BlackList);
