import {
  USER_SET,
  USER_RESET,
  USER_REQUEST,
  USER_RECEIVE
} from 'types';

export function set(payload) {
  return {
    type: USER_SET,
    payload,
  };
};

export function reset() {
  return {
    type: USER_RESET
  }
};

function fetchStatusHandler(response) {
  if (response.status === 200) {
    return response;
  } else {
    throw new Error(response.statusText);
  }
}

export function login(payload) {
  return (dispatch) => {
    dispatch({ type: USER_REQUEST });
    let uri = `${process.env.API_URL}/api/login`;

    console.log('[userAction:dispatch::login]', uri, JSON.stringify({
      user: payload.user,
      pass: payload.pass
    }));

    return fetch(uri, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        user: payload.user,
        pass: payload.pass
      })
    })
    .then(fetchStatusHandler)
    .then(res => res.json())
    .then(json => {
      return dispatch({
        type: USER_RECEIVE,
        logged: true,
        ...json
      });
    })
    .catch((err) => {
      return dispatch({
        type: USER_RECEIVE,
        logged: false
      });
    })
  };
}
