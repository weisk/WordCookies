import {
  WORDS_SET,
  WORDS_RESET
} from 'types';

export function set(payload) {
  return {
    type: WORDS_SET,
    payload,
  };
};

export function reset() {
  return {
    type: WORDS_RESET
  }
};
