import {
  ROUTERSTATE_SET,
  ROUTERSTATE_RESET
} from 'types';

export function set(payload) {
  return {
    type: ROUTERSTATE_SET,
    payload,
  };
};

export function reset() {
  return {
    type: ROUTERSTATE_RESET
  }
};
