import 'isomorphic-fetch';
import _ from 'lodash';

import {
  BLACKLIST_FETCH,
  BLACKLIST_RECEIVE,
  BLACKLIST_SET,
  BLACKLIST_RESET,
  BLACKLIST_SETALL
} from 'types';

export function blacklistFetch() {
  return (dispatch, getState) => {
    dispatch({ type: BLACKLIST_FETCH });
    const { token } = getState().user;
    const cfg = {
      headers: {
        Authorization: token
      }
    };
    return fetch(`${process.env.API_URL}/api/blacklist`, cfg)
    .then(res => res.json())
    .then(json => {
      let data = {
        type: BLACKLIST_RECEIVE,
        items: json,
        receivedAt: Date.now()
      };
      setTimeout(() => {
        dispatch(data);
      }, 100);
    });
  };
}

export function blacklistReceive(payload) {
  return {
    type: BLACKLIST_RECEIVE
  }
}

export function blacklistSet(word) {
  return (dispatch, getState) => {
    dispatch({ type: BLACKLIST_SET, word });
    const { token } = getState().user;
    const cfg = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': token
      },
      body: JSON.stringify({word}),
    };
    return fetch(`${process.env.API_URL}/api/blacklist`, cfg)
    .then(res => res.json())
    .then(json => {
      dispatch({
        type: BLACKLIST_RECEIVE,
        items: json,
        receivedAt: Date.now()
      });
    });
  };
}

export function blacklistSetAll(payload) {
  return {
    type: BLACKLIST_SETALL,
    payload
  }
}

export function reset() {
  return {
    type: BLACKLIST_RESET
  }
}
