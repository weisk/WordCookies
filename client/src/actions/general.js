import {
  GENERAL_SET,
  GENERAL_RESET
} from 'types';

export function set(payload) {
  return {
    type: GENERAL_SET,
    payload,
  };
};

export function reset() {
  return {
    type: GENERAL_RESET
  }
};
