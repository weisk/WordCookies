import 'isomorphic-fetch';
import _ from 'lodash';

import {
  APISTATS_FETCH,
  APISTATS_RECEIVE,
  APISTATS_SET,
  APISTATS_RESET
} from 'types';

export function apiStatsFetch() {
  return (dispatch) => {
    dispatch({ type: APISTATS_FETCH });
    return fetch(`${process.env.API_URL}`)
    .then(res => res.json())
    .then(json => {
      let data = {
        type: APISTATS_RECEIVE,
        lastUpdated: Date.now(),
        status: json.data
      };
      setTimeout(() => {
        dispatch(data);
      }, 100);
    });
  };
}

export function apiStatsReceive() {
  return {
    type: APISTATS_RECEIVE
  }
}

export function set(payload) {
  return {
    type: APISTATS_SET,
    payload
  }
}

export function reset() {
  return {
    type: APISTATS_RESET
  }
}
