import {
  USER_GET,
  USER_SET,
  USER_RESET,
  USER_REQUEST,
  USER_RECEIVE,
} from 'types';

import initialState from '../state';

const initialUserState = initialState.user;

export default function reducer(state={}, action) {
  switch (action.type) {
    case USER_GET:
      return state;
    case USER_SET:
      return {...state, ...action.payload};
    case USER_RESET:
      return {...initialUserState};
    case USER_REQUEST:
      return {
        ...state,
        loading: true
      };
    case USER_RECEIVE:
      return {
        ...state,
        ...action
      };
    default:
      return state;
  }
};

