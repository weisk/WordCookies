import {
  WORDS_SET,
  WORDS_RESET
} from 'types';

import initialState from '../state';

export default function reducer(state=initialState.wordsForm, action) {
  switch (action.type) {
    case WORDS_SET:
      return {...state, ...action.payload};
    case WORDS_RESET:
      return initialState.wordsForm;
    default:
      return state;
  }
};

