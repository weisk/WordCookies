import {
  APISTATS_FETCH,
  APISTATS_RECEIVE,
  APISTATS_SET,
  APISTATS_RESET
} from 'types';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case APISTATS_FETCH:
      return {
        ...state,
        isFetching: true
      };
    case APISTATS_RECEIVE:
      return {
        ...state,
        isFetching: false,
        lastUpdated: action.lastUpdated,
        status: action.status
      };
    case APISTATS_SET:
      return {
        ...state,
        ...action.payload
      };
    case APISTATS_RESET:
      return {
        ...initialState
      };
    default:
      return state
  }
}

