import {
  BLACKLIST_FETCH,
  BLACKLIST_RECEIVE,
  BLACKLIST_SET,
  BLACKLIST_SETALL,
  BLACKLIST_RESET
} from 'types';

export default function reducer(state = [], action) {
  switch (action.type) {
    case BLACKLIST_FETCH:
      return {
        ...state,
        isFetching: true
      };
    case BLACKLIST_RECEIVE:
      return {
        ...state,
        isFetching: false,
        items: action.items,
        receivedAt: action.receivedAt,
      };
    case BLACKLIST_SET:
      return state;
      // return {
      //   ...state,
      //   items: [
      //     ...state.items,
      //     action.word
      //   ]
      // }
    case BLACKLIST_SETALL:
      return {
        ...state,
        items: action.words
      }
    case BLACKLIST_RESET:
      return initialState;
    default:
      return state
  }
}

