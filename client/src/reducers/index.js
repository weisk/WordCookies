import { combineReducers } from 'redux';

import user from './user';
import apiStats from './apiStats';
import general from './general';
import blacklist from './blacklist';
import wordsForm from './wordsForm';

export {
  user,
  apiStats,
  general,
  blacklist,
  wordsForm
};

export default combineReducers({
  user,
  apiStats,
  general,
  blacklist,
  wordsForm
});
