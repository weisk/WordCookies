import {
  GENERAL_GET,
  GENERAL_SET,
  GENERAL_RESET
} from 'types';

export default function reducer(state={}, action) {
  switch (action.type) {
    case GENERAL_GET:
      return state;
    case GENERAL_SET:
      return {...state, ...action.payload};
    case GENERAL_RESET:
      return {...initialState};
    default:
      return state;
  }
};

